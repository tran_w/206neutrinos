#!/usr/bin/env python3

##      206neutrinos
#       EPITECH 2012-2013 Math project
#
#       @author tran_w;naina_r

import  sys
import  math

#       Check if the parameters given are numbers.
def     checkArgs(argv):
    try:
        nb = float(argv)
        if nb < 0:
            raise RuntimeError('Argument skal være overlegen i forhold til\
nul')
    except ValueError:
        raise RuntimeError('Argumenter skal være et tal.')
    return True

#       Calculate the quadratic mean
def     calcQuadraticMean(nbMeasure, standardDeviation, arithmeticMean, value):
    quadraticMean = math.sqrt((((standardDeviation ** 2.0 + \
        arithmeticMean ** 2.0) * (nbMeasure - 1.0)) + value ** 2.0) \
        / nbMeasure)
    return quadraticMean

#       Calculate the arithmetic mean
def     calcArithmeticMean(nbMeasure, arithmeticMean, value):
    arithmeticMean = ((arithmeticMean * (nbMeasure - 1.0)) + value) \
        / nbMeasure
    return arithmeticMean

#       Calculate the harmonic mean
def     calcHarmonicMean(nbMeasure, harmonicMean, value):
    harmonicMean = nbMeasure / ((1.0 / harmonicMean * \
        (nbMeasure - 1.0)) + (1.0 / value))
    return harmonicMean

#       Calculate the standard deviation
def     calcStandardDeviation(nbMeasure, standardDeviation, arithmeticMean, value):
    standardDeviation = math.sqrt((((((standardDeviation ** 2.0) + \
        (arithmeticMean ** 2.0)) * (nbMeasure - 1.0)) + (value ** 2.0)) \
        / nbMeasure) - (float(((arithmeticMean * (nbMeasure - 1.0)) \
        + value) / nbMeasure) ** 2.0))
    return standardDeviation

#       Print the result
def     showResult(nbMeasure, arithmeticMean, harmonicMean,
    standardDeviation, quadraticMean):
    print("\tAntal malinder : " + str(nbMeasure))
    print("\tStandardafvilgelse : %0.2f" % standardDeviation)
    print("\tAritmetisk gennemsnit : %0.2f" % arithmeticMean)
    print("\tKvadratisk gennemsnit : %0.2f" % quadraticMean)
    print("\tHarmonisk gennemsnit : %0.2f" % harmonicMean)
    print("")

#       Get user input
def     getInput():
    inputValue = input("indtast din vaerdi : ")
    if inputValue == "ende":
        return "ende"
    checkArgs(inputValue)
    return (inputValue)

#       Program's loop. It gets the value given by the user on standard input,
#       calculate and print the result
def     loop(argv):
    inputValue = ""
    nbMeasure = float(argv[1])
    arithmeticMean = float(sys.argv[2])
    harmonicMean = float(sys.argv[3])
    standardDeviation = float(sys.argv[4])
    while 42:
        inputValue = getInput()
        if inputValue == "ende":
            break
        nbMeasure += 1
        quadraticMean = calcQuadraticMean(nbMeasure, standardDeviation, \
            arithmeticMean, float(inputValue))
        standardDeviation = calcStandardDeviation(nbMeasure, standardDeviation, \
            arithmeticMean, float(inputValue))
        arithmeticMean = calcArithmeticMean(nbMeasure, arithmeticMean, \
            float(inputValue))
        harmonicMean = calcHarmonicMean(nbMeasure, harmonicMean, \
            float(inputValue))
        showResult(nbMeasure, arithmeticMean, harmonicMean, \
            standardDeviation, quadraticMean)

#       Main
def     main():
    try:
        if len(sys.argv) < 3 or len(sys.argv) > 5:
            print("Brug: ./206neutrinos <Antallet af målingerne> <aritmetiske> \
<harmoniske middelværdi> <SD>")
        else:
            for i in range(1, len(sys.argv)):
                if checkArgs(sys.argv[i]) is False:
                    return False
            loop(sys.argv)
        return True
    except Exception as err:
        sys.stderr.write("ERROR: %s\nProgram er spændende nu.\n" % str(err))
        return False

if __name__ == '__main__':
    sys.exit(main())
